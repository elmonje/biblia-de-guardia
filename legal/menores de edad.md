# Menores de edad #

## Definición ##
- Código Civil y Comercial de la Nación, Cap 2, art 25: Menor de edad es la persona que no ha cumplido 18 años ... adolescente a la persona menor de edad que cumplió 13 años.

## Consentimiento ##
- CCCN, art 26: Se presume que el adolescente entre 13 y 18 años tiene aptitud para decidir por si respecto de aquellos tratamientos que no resultan invasivos ... A partir de los 16 años ... es considerado como un adulto para las decisiones atinentes al cuidado de su propio cuerpo.
- Decreto Reglamentario (CABA) 2316/03, art. 4, inc. h: (...) Se presume que todo niño o adolescente que requiere atención en un servicio de salud está en condiciones de formar un juicio propio y tiene suficiente razón y madurez para ello; en especial tratándose del ejercicio de derechos personalísimos (... solicitar testeo de VIH, solicitar provisión de anticonceptivos)

_Tomado de: Manual de medicina legal y deontología médica (2da edicion - 2016). Covelli JL, Pasquariello A, Casas Parera I._